package com.example.steffen.movs_retrofit;

/**
 * Created by Steffen on 02.03.2017.
 */

public class ApiUtils {
    static final String API_URL = "http://api.icndb.com";

    public static GetNewChuckApiInterface getApi() {
        return RetrofitClient.getClient(API_URL).create(GetNewChuckApiInterface.class);
    }

}
