package com.example.steffen.movs_retrofit;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button bt_sync, bt_async;
    TextView tv_content;
    Switch sw_name;
    EditText et_firstName, et_lastName;
    GetNewChuckApiInterface api;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        api = ApiUtils.getApi();

        sw_name = (Switch) findViewById(R.id.sw_name);
        et_firstName = (EditText) findViewById(R.id.et_firstName);
        et_lastName = (EditText) findViewById(R.id.et_lastName);
        tv_content = (TextView) findViewById(R.id.tv_content);
        bt_sync = (Button) findViewById(R.id.bt_sync);
        bt_async = (Button) findViewById(R.id.bt_async);

        bt_sync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNewChuck(true);
            }
        });

        bt_async.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadNewChuck(false);
            }
        });


    }


    public void loadNewChuck(Boolean sync){
        Call<ChuckJoke> call = null;
        if (sw_name.isChecked()) {
            String firstName = et_firstName.getText().toString();
            String lastName = et_lastName.getText().toString();
            call = api.getCustomJoke(firstName, lastName);
        } else {
           call = api.getJoke();
        }

        if (sync){
            new LoadData().execute(call);
        } else {
            makeAsyncCall(call);
        }
    }


    private class LoadData extends AsyncTask<Call, Void, ChuckJoke> {
        @Override
        protected ChuckJoke doInBackground(Call... params) {
            ChuckJoke cj = null;
            try {
                cj = (ChuckJoke) params[0].execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return cj;
        }

        @Override
        protected void onPostExecute(ChuckJoke cj) {
            tv_content.setText(cj.getValue().getJoke());
        }
    }


    public void makeAsyncCall(Call call) {
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if(response.isSuccessful()) {
                    parseResponse((ChuckJoke) response.body());
                } else {
                    int statusCode  = response.code();
                    // handle request errors depending on status code
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("MainActivity", "error loading from API");
            }
        });
    }

    public void parseResponse(final ChuckJoke cj) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tv_content.setText(cj.getValue().getJoke());
            }
        });
    }

}
