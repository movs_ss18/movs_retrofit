package com.example.steffen.movs_retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Steffen on 02.03.2017.
 */

public interface GetNewChuckApiInterface {

    @GET("/jokes/random")
    Call<ChuckJoke> getJoke();

    @GET("/jokes/random")
    Call<ChuckJoke> getCustomJoke(@Query("firstName") String firstName, @Query("lastName") String lastName);

}
